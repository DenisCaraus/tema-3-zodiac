#include "RequestsManager.h"

#include "Tema3Request.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("Tema3", std::make_shared<Tema3Request>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
