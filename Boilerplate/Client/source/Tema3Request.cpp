#include "Tema3Request.h"

#include<iostream>

Tema3Request::Tema3Request() : Request("Tema3")
{
	std::string date;

	std::cout << "Introduceti o data (format luna/zi/an) \n";
	std::cin >> date;

	this->content.push_back(boost::property_tree::ptree::value_type("Date", date));
}