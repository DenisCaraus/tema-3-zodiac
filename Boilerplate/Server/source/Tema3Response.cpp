#include "Tema3Response.h"

#include <fstream>

#include "Date.h"

Tema3Response::Tema3Response()
{

}

std::string Tema3Response::interpretPacket(const boost::property_tree::ptree & packet)
{
	std::string input = packet.get<std::string>("Date");

	Date date(input);

	std::string result = "Data incorecta!";

	if (date.isValid())
	{
		result = date.getStarSign();
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "tema3.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

	return this->getContentAsString();
}
