#include "ResponsesManager.h"
#include "Tema3Response.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Tema3", std::make_shared<Tema3Response>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
