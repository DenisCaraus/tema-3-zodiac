#include "Date.h"

#include<boost/tokenizer.hpp>

Date::Date(const std::string& date)
{
	isValidDate = true;

	boost::char_separator<char> sep("/", "", boost::keep_empty_tokens);
	boost::tokenizer<boost::char_separator<char> > tokens(date, sep);
	boost::tokenizer<boost::char_separator<char> >::iterator it = tokens.begin();

	for (int index = 0; it != tokens.end(); ++it, ++index)
	{
		switch (index)
		{
		case 0: month = atoi((*it).c_str()); break;
		case 1: day = atoi((*it).c_str()); break;
		case 2: year = atoi((*it).c_str()); break;
		case 3: isValidDate = false; break;
		}
	}

	if (isValidDate)
	{
		validate();
	}
}


Date::~Date()
{
}

uint8_t Date::getDay()
{
	return day;
}

uint8_t Date::getMonth()
{
	return month;
}

uint16_t Date::getYear()
{
	return year;
}

bool Date::isValid()
{
	return isValidDate;
}

bool Date::isLeapYear()
{
	return isLeap;
}

std::string Date::getStarSign()
{
	switch (month)
	{
	case 1:
	{
		if (day < 20)
		{
			return "Capricorn(22 decembrie - 19 ianuarie)";
		}
		return "Varsator(20 ianuarie - 18 februarie)";
	}
	case 2:
	{
		if (day < 19)
		{
			return "Varsator(20 ianuarie - 18 februarie)";
		}
		return "Pesti(19 februarie - 20 martie)";
	}
	case 3:
	{
		if (day < 21)
		{
			return "Pesti(19 februarie - 20 martie)";
		}
		return "Berbec(21 martie - 20 aprilie)";
	}
	case 4:
	{
		if (day < 21)
		{
			return "Berbec(21 martie - 20 aprilie)";
		}
		return "Taur(21 aprilie - 21 mai)";
	}
	case 5:
	{
		if (day < 22)
		{
			return "Taur(21 aprilie - 21 mai)";
		}
		return "Gemeni(22 mai - 21 iunie)";
	}
	case 6:
	{
		if (day < 22)
		{
			return "Gemeni(22 mai - 21 iunie)";
		}
		return "Rac(22 iunie - 22 iulie)";
	}
	case 7:
	{
		if (day < 23)
		{
			return "Rac(22 iunie - 22 iulie)";
		}
		return "Leu(23 iulie - 22 august)";
	}
	case 8:
	{
		if (day < 23)
		{
			return "Leu(23 iulie - 22 august)";
		}
		return "Fecioara(23 august - 22 septembrie)";
	}
	case 9:
	{
		if (day < 23)
		{
			return "Fecioara(23 august - 22 septembrie)";
		}
		return "Balanta(23 septembrie - 22 octombrie)";
	}
	case 10:
	{
		if (day < 23)
		{
			return "Balanta(23 septembrie - 22 octombrie)";
		}
		return "Scorpion(23 octombrie - 21 noiembrie)";
	}
	case 11:
	{
		if (day < 22)
		{
			return "Scorpion(23 octombrie - 21 noiembrie)";
		}
		return "Sagetator(22 noiembrie - 21 decembrie)";
	}
	case 12:
	{
		if (day < 22)
		{
			return "Sagetator(22 noiembrie - 21 decembrie)";
		}
		return "Capricorn(22 decembrie - 19 ianuarie)";
	}
	}

	return "";
}

void Date::validate()
{
	isLeap = false;
	uint8_t maxDayValue = 31;

	if ((month > 12) || (month < 1))
	{
		isValidDate = false;
		return;
	}

	if (year % 4 == 0)
	{
		isLeap = true;
	}

	switch (month)
	{
	case 2:
	{
		if (isLeap)
		{
			maxDayValue = 29;
			break;
		}
		maxDayValue = 28;
		break;
	}
	case 4: maxDayValue = 30; break;
	case 6: maxDayValue = 30; break;
	case 9: maxDayValue = 30; break;
	case 11: maxDayValue = 30; break;
	}

	if ((day > maxDayValue) || (day < 1))
	{
		isValidDate = false;
		return;
	}
}
