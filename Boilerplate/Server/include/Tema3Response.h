#pragma once

#include "Response.h"

class Tema3Response : public Framework::Response
{
public:
	Tema3Response();

	std::string interpretPacket(const boost::property_tree::ptree& packet);

};