#pragma once

#include<string>

class Date
{
public:
	Date(const std::string& date);
	~Date();

	uint8_t getDay();
	uint8_t getMonth();
	uint16_t getYear();
	bool isValid();
	bool isLeapYear();

	std::string getStarSign();
private:
	void validate();

private:
	uint8_t day, month;
	uint16_t year;
	bool isValidDate;
	bool isLeap;
};

